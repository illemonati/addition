import keras
import tensorflow.keras as keras
import random
import tensorflowjs as tfjs


def build_model():
    model = keras.models.Sequential([
        keras.layers.Dense(2, input_shape=[2], activation='relu'),
        keras.layers.Dense(24, activation='relu'),
        keras.layers.Dense(24, activation='relu'),
        keras.layers.Dense(1, activation='linear')
    ])
    return model


x = []
for i in range(0, 100):
    for j in range(0, 100):
        x.append((i, j))
y = [a + b for (a, b) in x]

# for i, (a, b) in enumerate(x):
#     print("{} + {} = {}".format(a, b, y[i]))

model = build_model()
model.compile(optimizer='adam', loss='mae', metrics=['categorical_accuracy'])

model.fit(x, y, epochs=1000, verbose=1)
model.save('model.hdf5')


