import keras
import tensorflow.keras as keras
import random

x = []
for i in range(0, 100):
    for j in range(0, 100):
        x.append((i, j))
y = [a + b for (a, b) in x]
model = keras.models.load_model('model.hdf5')

print("results:")


def basic_test():
    for (a, b) in x:
        p = model.predict([(a, b)])[0][0]
        print("{} + {} = rounded: {} predicted: {}".format(a, b, round(p), p))


def advanced_test():
    random_nums = []
    for _ in range(0, 50):
        random_nums.append(random.randint(0, 1000))
    random_as = random_nums[:len(random_nums)//2]
    random_bs = random_nums[len(random_nums)//2:]
    for a, b in zip(random_as, random_bs):
        p = model.predict([(a, b)])[0][0]
        print("{} + {} = rounded: {} predicted: {} actual: {}".format(a, b, round(p), p, a+b))

# basic_test()
advanced_test()
